#![warn(rust_2018_idioms)]


use tokio_util::codec::{Decoder, Encoder};
use tokio_serial::SerialPort;  // working set dtr не видел потому что не импортировали....

use futures::stream::StreamExt;
use futures::SinkExt;
use std::{env, io, str};


use bytes::BytesMut;
use std::time::Duration;
use std::thread;



#[cfg(unix)]
const DEFAULT_TTY: &str = "/dev/ttyUSB0";
#[cfg(windows)]
const DEFAULT_TTY: &str = "COM3";

struct LineCodec;

impl Decoder for LineCodec {
    type Item = String;
    type Error = io::Error;

    fn decode(&mut self, src: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        let newline = src.as_ref().iter().position(|b| *b == b'\n');
        if let Some(n) = newline {
            let line = src.split_to(n + 1);
            return match str::from_utf8(line.as_ref()) {
                Ok(s) => Ok(Some(s.to_string())),
                Err(_) => Err(io::Error::new(io::ErrorKind::Other, "Invalid String")),
            };
        }
        Ok(None)
    }
}

impl Encoder for LineCodec {
    type Item = String;
    type Error = io::Error;

    fn encode(&mut self, _item: Self::Item, _dst: &mut BytesMut) -> Result<(), Self::Error> {
		_dst.extend_from_slice(b"i\r\n");
		println!("encode");
        Ok(())
    }
}

#[tokio::main]
async fn main() {
    let mut args = env::args();
    let tty_path = args.nth(1).unwrap_or_else(|| DEFAULT_TTY.into());

    let mut settings = tokio_serial::SerialPortSettings::default();
	settings.timeout=Duration::from_millis(100);
	//println!("{}",settings.timeout);
    let mut port = tokio_serial::Serial::from_path(tty_path, &settings).unwrap();

	//tokio_serial::Serial::write_data_terminal_ready(&mut port,false);  //working
	port.write_data_terminal_ready(false).expect("DTR false");
	
	
	
	println!("Begin");
	println!("{:?}",settings);
	
    #[cfg(unix)]
    port.set_exclusive(false)
        .expect("Unable to set serial port exclusive to false");

  //	port.write(b"i\r\n");  // use std::io::Write
   
  // let mut reader = LineCodec.framed(port);
  
	let (mut writer, mut reader) = LineCodec.framed(port).split();
	thread::sleep(Duration::from_millis(1450));

	writer.send("d\r\n".to_string()).await.expect("send err");
	println!("Sended: d\\r\\n");
	
    while let Some(line_result) = reader.next().await {
        
		let line = line_result.expect("Failed to read line");
		let line_new=line.replace("\r\n","\\r\\n");
        println!("Received: {}", line_new);
    }
}